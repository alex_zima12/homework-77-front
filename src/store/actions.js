import {FETCH_MESSAGE_SUCCESS,POST_MESSAGE_ERROR} from "./actionTypes";
import axios from "../axiosApi";

const fetchMessageSuccess = (messages) => {
    return {type: FETCH_MESSAGE_SUCCESS, messages}
};

export const fetchMessages = () => {
    return async dispatch => {
        const response = await axios.get("/messages");
        dispatch(fetchMessageSuccess(response.data));
    };
};

const postMessageError = error => {
    return {type: POST_MESSAGE_ERROR, error}
};

export const postMessage = messageData => {
    return async dispatch => {
        try {
            await axios.post("/messages", messageData)
            dispatch(fetchMessages());
        } catch (e) {
            console.log(e.response);
            dispatch(postMessageError(e.response.data.error))
        }
    };
};


