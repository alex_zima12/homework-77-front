import React, {useEffect, useState} from 'react';
import {useDispatch, useSelector} from "react-redux";
import {fetchMessages,postMessage} from "../../store/actions";
import NewMessage from "../../components/NewMessage";

const Thread = () => {
    const dispatch = useDispatch();
    const messages = useSelector(state => state.messages);
    const error = useSelector(state => state.error);
    const [state, setState] = useState({
        author: "",
        message: "",
        image: ""
    });

    useEffect(() => {
        dispatch(fetchMessages());
    }, [dispatch]);

    const inputChangeHandler = e => {
        const name = e.target.name;
        const value = e.target.value;
        setState(prevState => {
            return {...prevState, [name]: value};
        });
    };

    const fileChangeHandler = e => {
        const name = e.target.name;
        const file = e.target.files[0];
        setState(prevState => ({...prevState, [name]: file}));
    };

    const submitFormHandler = e => {
        e.preventDefault();
        const formData = new FormData();
        Object.keys(state).forEach(key => {
            formData.append(key, state[key]);
        });
        dispatch(postMessage(formData));
    };

    let arr = [];
    messages.map((mes)=> {
        let newMes = <NewMessage
            key={messages.indexOf(mes)}
            author={mes.author}
            image={mes.image}
            message={mes.message}/>;
        return arr.push(newMes)
    });

    let errorMessage = '';
    if (error) {
        errorMessage = <h1>{error}</h1>
        return errorMessage;
    }

    return (
        <div className="container bg-gradient-info">
            {errorMessage}
            {arr}
            <form
                className="p-5"
                onSubmit={submitFormHandler}
            >
                <div className="input-group mb-3">
                    <div className="input-group-prepend">
                        <span className="input-group-text" id="basic-addon1">Author</span>
                    </div>
                    <input type="text"
                           className="form-control"
                           id="author"
                           placeholder="Username"
                           onChange={inputChangeHandler} name="author"/>
                </div>
                <div className="input-group mb-3">
                    <div className="input-group-prepend">
                        <span className="input-group-text">Message</span>
                    </div>
                    <textarea className="form-control"
                              placeholder="Message"
                              onChange={inputChangeHandler}
                              name="message"
                              required
                    />
                </div>
                <div className="input-group mb-3 ">
                    <label htmlFor="FormControlFile1">Add file</label>
                    <input
                        type="file"
                        name="image"
                        className="form-control-file"
                        id="FormControlFile1"
                        onChange={fileChangeHandler}
                    />
                </div>
                <button
                    type="submit"
                    className="btn btn-info"
                >Send message</button>
            </form>
        </div>
    );
};

export default Thread;