import React from 'react';
import {apiURL} from "../constans";

const NewMessage = ({author, message, image}) => {

    let cardImage;
    if (image) {
        cardImage = <img src={apiURL + "/uploads/" + image} className="img-thumbnail" alt="threadImg"/>
    }

    return (
        <div aria-live="assertive" aria-atomic="true" className="alert alert-secondary" role="alert">
            <div className="toast-header">
                <strong className="mr-auto">{author}</strong>
            </div>
            <div className="toast-body">
                {message}
            </div>
            {cardImage}
        </div>
    );
};

export default NewMessage;